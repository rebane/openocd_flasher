#ifndef _FLASHER_H_
#define _FLASHER_H_

#include <stdint.h>

#define FLASHER_COMMAND_READ_ID          0
#define FLASHER_COMMAND_MASS_ERASE       1
#define FLASHER_COMMAND_SECTOR_ERASE     2
#define FLASHER_COMMAND_READ             3
#define FLASHER_COMMAND_WRITE            4
#define FLASHER_COMMAND_VERIFY           5

#define flasher_start_get(base)          (*(volatile uint32_t *)((base) + 0x00))
#define flasher_command_get(base)        (*(volatile uint32_t *)((base) + 0x04))
#define flasher_status_get(base)         (*(volatile uint32_t *)((base) + 0x08))
#define flasher_param_get(base)          (*(volatile uint32_t *)((base) + 0x0C))
#define flasher_offset_get(base)         (*(volatile uint32_t *)((base) + 0x10))
#define flasher_len_get(base)            (*(volatile uint32_t *)((base) + 0x14))

#define flasher_start_set(base, value)   do { (*(volatile uint32_t *)((base) + 0x00)) = (value); }while(0)
#define flasher_command_set(base, value) do { (*(volatile uint32_t *)((base) + 0x04)) = (value); }while(0)
#define flasher_status_set(base, value)  do { (*(volatile uint32_t *)((base) + 0x08)) = (value); }while(0)
#define flasher_param_set(base, value)   do { (*(volatile uint32_t *)((base) + 0x0C)) = (value); }while(0)
#define flasher_offset_set(base, value)  do { (*(volatile uint32_t *)((base) + 0x10)) = (value); }while(0)
#define flasher_len_set(base, value)     do { (*(volatile uint32_t *)((base) + 0x14)) = (value); }while(0)

#define flasher_data_ptr(base)           ((void *)((volatile uint8_t *)((base) + 0x20)))

#endif

