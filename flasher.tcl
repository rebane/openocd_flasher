set flasher_command_read_id      0
set flasher_command_mass_erase   1
set flasher_command_sector_erase 2
set flasher_command_read         3
set flasher_command_write        4
set flasher_command_verify       5

set flasher_buffer_size          0
set flasher_sector_size          0
set flasher_auto_erase           0
set flasher_auto_verify          0

proc flasher_init {} {
	flasher_target_wait
	flasher_target_read_id
}

proc flasher_target_mrw {reg} {
	mem2array value 32 $reg 1
	return $value(0)
}

proc flasher_target_wait {} {
	global flasher_buffer

	while {[flasher_target_mrw [expr {$flasher_buffer + 0x00}]]} { }
}

proc flasher_target_read_id {} {
	global flasher_command_read_id
	global flasher_buffer
	global flasher_buffer_size
	global flasher_sector_size

	mww [expr {$flasher_buffer + 0x04}] $flasher_command_read_id
	mww [expr {$flasher_buffer + 0x08}] 0x00000000
	mww [expr {$flasher_buffer + 0x00}] 0x00000001
	flasher_target_wait
	set id [flasher_target_mrw [expr {$flasher_buffer + 0x0C}]]
	set manufacturer_id [format "0x%02X" [expr {($id >> 16) & 0xFF}]]
	set memory_type [format "0x%02X" [expr {($id >> 8) & 0xFF}]]
	set memory_capacity [expr {2 ** [expr {($id >> 0) & 0xFF}]}]
	set flasher_sector_size [flasher_target_mrw [expr {$flasher_buffer + 0x10}]]
	echo "flasher: manufacturer ID: $manufacturer_id, memory type: $memory_type, memory capacity: $memory_capacity bytes, sector size: $flasher_sector_size bytes"
	set flasher_buffer_size [flasher_target_mrw [expr {$flasher_buffer + 0x14}]]
	echo "flasher: target buffer size: $flasher_buffer_size"
}

proc flasher_target_read_block {offset len} {
	global flasher_command_read
	global flasher_buffer

	mww [expr {$flasher_buffer + 0x04}] $flasher_command_read
	mww [expr {$flasher_buffer + 0x08}] 0x00000000
	mww [expr {$flasher_buffer + 0x10}] $offset
	mww [expr {$flasher_buffer + 0x14}] $len
	mww [expr {$flasher_buffer + 0x00}] 0x00000001
	flasher_target_wait
	set status [flasher_target_mrw [expr {$flasher_buffer + 0x08}]]
	if {[expr {$status > 0}]} {
		error "read error, offset $offset"
	}
}

proc flasher_target_write_block {offset len} {
	global flasher_command_write
	global flasher_buffer

	mww [expr {$flasher_buffer + 0x04}] $flasher_command_write
	mww [expr {$flasher_buffer + 0x08}] 0x00000000
	mww [expr {$flasher_buffer + 0x10}] $offset
	mww [expr {$flasher_buffer + 0x14}] $len
	mww [expr {$flasher_buffer + 0x00}] 0x00000001
	flasher_target_wait
	set status [flasher_target_mrw [expr {$flasher_buffer + 0x08}]]
	if {[expr {$status > 0}]} {
		error "write error, offset $offset"
	}
}

proc flasher_target_verify_block {offset len} {
	global flasher_command_verify
	global flasher_buffer

	mww [expr {$flasher_buffer + 0x04}] $flasher_command_verify
	mww [expr {$flasher_buffer + 0x08}] 0x00000000
	mww [expr {$flasher_buffer + 0x10}] $offset
	mww [expr {$flasher_buffer + 0x14}] $len
	mww [expr {$flasher_buffer + 0x00}] 0x00000001
	flasher_target_wait
	set status [flasher_target_mrw [expr {$flasher_buffer + 0x08}]]
	if {[expr {$status > 0}]} {
		set status [flasher_target_mrw [expr {$flasher_buffer + 0x0C}]]
		set status [expr {$status + $offset}]
		error "verify error, offset $status"
	}
}

proc flasher_target_mass_erase {} {
	global flasher_command_mass_erase
	global flasher_buffer

	mww [expr {$flasher_buffer + 0x04}] $flasher_command_mass_erase
	mww [expr {$flasher_buffer + 0x08}] 0x00000000
	mww [expr {$flasher_buffer + 0x00}] 0x00000001
	flasher_target_wait
}

proc flasher_target_sector_erase {offset} {
	global flasher_command_sector_erase
	global flasher_buffer

	mww [expr {$flasher_buffer + 0x04}] $flasher_command_sector_erase
	mww [expr {$flasher_buffer + 0x08}] 0x00000000
	mww [expr {$flasher_buffer + 0x10}] $offset
	mww [expr {$flasher_buffer + 0x00}] 0x00000001
	flasher_target_wait
}

proc flasher_read_array {loc size} {
	global flasher_buffer
	global flasher_buffer_size

	set flasher_offset $loc

	for {set offset 0} {$offset < $size} {set offset [expr {$offset + $flasher_buffer_size}]} {
		set len [expr {$size - $offset}]
		if {[expr {$len > $flasher_buffer_size}]} {
			set len $flasher_buffer_size
		}
		set flasher_offset [expr {$loc + $offset}]
		set flasher_percent [expr {$offset * 100 / $size}]
		echo -n "\rflasher: read offset $flasher_offset, $flasher_percent% "
		flasher_target_read_block $flasher_offset $len
		mem2array buffer 8 [expr {$flasher_buffer + 0x20}] $len
		for {set i 0} {$i < $len} {set i [expr {$i + 1}]} {
			set data([expr $offset + $i]) $buffer($i)
		}
	}
	echo "\rflasher: read offset $flasher_offset, 100% "
	echo "flasher: read $size bytes from $loc"

	return $data
}

proc flasher_write_array {data loc} {
	global flasher_buffer
	global flasher_buffer_size
	global flasher_sector_size
	global flasher_auto_erase
	global flasher_auto_verify

	set auto_erase_sector 0xFFFFFFFF
	set sector 0
	set size [array size data]
	set flasher_offset $loc

	for {set offset 0} {$offset < $size} {set offset [expr {$offset + $flasher_buffer_size}]} {
		set len [expr {$size - $offset}]
		if {[expr {$len > $flasher_buffer_size}]} {
			set len $flasher_buffer_size
		}
		set flasher_offset [expr {$loc + $offset}]
		set flasher_percent [expr {$offset * 100 / $size}]
		echo -n "\rflasher: write offset $flasher_offset, $flasher_percent% "

		set buffer ""
		for {set i 0} {$i < $len} {set i [expr {$i + 1}]} {
			set buffer($i) $data([expr $offset + $i])
		}
		array2mem buffer 8 [expr {$flasher_buffer + 0x20}] $len

		if {[expr {$flasher_auto_erase != 0}]} {
			for {set i $flasher_offset} {$i < [expr {$flasher_offset + $len}]} {incr i} {
				set sector [expr {$i / $flasher_sector_size}]
				if {[expr {$auto_erase_sector != $sector}]} {
					flasher_target_sector_erase [expr {$sector * $flasher_sector_size}]
					set auto_erase_sector $sector
				}
			}
		}
		flasher_target_write_block $flasher_offset $len
		if {[expr {$flasher_auto_verify != 0}]} {
			flasher_target_verify_block $flasher_offset $len
		}
	}
	echo "\rflasher: write offset $flasher_offset, 100% "
	echo "flasher: wrote $size bytes to $loc"
}

proc flasher_verify_array {data loc} {
	global flasher_buffer
	global flasher_buffer_size

	set size [array size data]
	set flasher_offset $loc

	for {set offset 0} {$offset < $size} {set offset [expr {$offset + $flasher_buffer_size}]} {
		set len [expr {$size - $offset}]
		if {[expr {$len > $flasher_buffer_size}]} {
			set len $flasher_buffer_size
		}
		set flasher_offset [expr {$loc + $offset}]
		set flasher_percent [expr {$offset * 100 / $size}]
		echo -n "\rflasher: verify offset $flasher_offset, $flasher_percent% "

		set buffer ""
		for {set i 0} {$i < $len} {set i [expr {$i + 1}]} {
			set buffer($i) $data([expr $offset + $i])
		}
		array2mem buffer 8 [expr {$flasher_buffer + 0x20}] $len

		flasher_target_verify_block $flasher_offset $len
	}
	echo "\rflasher: verify offset $flasher_offset, 100% "
}

proc flasher_auto_erase {on} {
	global flasher_auto_erase

	if {[expr {$on != 0}]} {
		set flasher_auto_erase 1
		echo "flasher: auto erase on"
	} else {
		set flasher_auto_erase 0
		echo "flasher: auto erase off"
	}
}

proc flasher_auto_verify {on} {
	global flasher_auto_verify

	if {[expr {$on != 0}]} {
		set flasher_auto_verify 1
		echo "flasher: auto verify on"
	} else {
		set flasher_auto_verify 0
		echo "flasher: auto verify off"
	}
}

proc flasher_read {local_filename loc size} {
	set data [flasher_read_array $loc $size]
	set datalen [array size data]

	for {set i 0} {$i < $datalen} {set i [expr {$i + 1}]} {
		pack b $data($i) -intle 8 [expr {$i * 8}]
	}

	set fd [open $local_filename w]
	fconfigure $fd -translation binary
	puts -nonewline $fd $b
	close $fd
	echo "flasher: wrote $datalen bytes into file \"$local_filename\""
}

proc flasher_write {local_filename loc} {
	set fd [open $local_filename r]
	fconfigure $fd -translation binary
	set b [read $fd]
	close $fd

	set datalen [string length $b]

	for {set i 0} {$i < $datalen} {set i [expr {$i + 1}]} {
		set data($i) [unpack $b -intle [expr {$i * 8}] 8]
	}

	echo "flasher: read $datalen bytes from file \"$local_filename\""
	flasher_write_array $data $loc
}

proc flasher_verify {local_filename loc} {
	set fd [open $local_filename r]
	fconfigure $fd -translation binary
	set b [read $fd]
	close $fd

	set datalen [string length $b]

	for {set i 0} {$i < $datalen} {set i [expr {$i + 1}]} {
		set data($i) [unpack $b -intle [expr {$i * 8}] 8]
	}

	echo "flasher: read $datalen bytes from file \"$local_filename\""
	flasher_verify_array $data $loc
}

